<?php
/*
Plugin Name: REST API Enhancements
Plugin URI:  
Description: Plugin to enhace WP REST API 
Version:     1.0
Author:      Teravision Technologies
Author URI:  https://www.teravisiontech.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: rest-enhancer
Domain Path: /languages
*/

defined( 'ABSPATH' ) or die( 'Operation not allowed!' );

// Absolute route
if ( !defined('REST_ENHANCER_PATH'))
define('REST_ENHANCER_PATH', plugin_dir_path(__FILE__));

use rest_enhancer\Rest_Enhancer_Logger;
use rest_enhancer\Rest_Enhancer_Caching;

function rest_enhancer_initialize() {
    // Load text domain
    load_plugin_textdomain( 'rest-enhancer', false, basename( dirname( __FILE__ ) ) . '/languages' );

    // Load dependencies
    require_once REST_ENHANCER_PATH . "includes/class-rest-enhancer-caching-interface.php";
    require_once REST_ENHANCER_PATH . "includes/class-rest-enhancer-caching.php";
    require_once REST_ENHANCER_PATH . "includes/class-rest-enhancer-utils.php";
    require_once REST_ENHANCER_PATH . "includes/class-rest-enhancer-logger.php";

    // Initializes options
    // rest_cache_time, rest_cache_datetime, rest_enhancer_cache_timeout

    // Run logger
    $rest_enhancer_logger = new Rest_Enhancer_Logger();
    $rest_enhancer_logger->load();

    // Run
    $rest_enhancer_caching = new Rest_Enhancer_Caching();
    $rest_enhancer_caching->load();

}
add_action('init', 'rest_enhancer_initialize');