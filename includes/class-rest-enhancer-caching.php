<?php
/**
 * @package rest_enhancer
 * @author Teravision Technologies
 * @version 1.0
 */
namespace rest_enhancer;

defined( 'ABSPATH' ) or die( 'Operation not allowed!' );
if ( !class_exists("rest_enhancer_Caching")) {

    /**
     * Class RaenhCaching
     * This class caches WP REST API endpoints results to
     * improve application performance
     * @package raenh
     */
    class Rest_Enhancer_Caching implements Rest_Enhancer_Caching_Interface
    {

        /**
         * Lock is used to prevent processing multiple requests
         * @var bool
         */
        private $lock;

        /**
         * Cache control related headers
         * @var array
         */
        private $cache_control_headers;

        /**
         * rest_enhancer_Caching constructor.
         */
        public function __construct()
        {

            $this->lock = false;

            $this->cache_control_headers = array(
                'Cache-Control' => 'public, max-age=3600'
            );

        }

        public function load()
        {
            add_filter('rest_pre_dispatch', array($this, 'rest_enhancer_pre_dispatch'), 10, 3);
        }


        public function rest_enhancer_pre_dispatch($result, $server, $request)
        {

            $request_uri = esc_url(Rest_Enhancer_Utils::get_server_var('REQUEST_URI'));
            $method = Rest_Enhancer_Utils::get_server_var('REQUEST_METHOD');

            // Send cache control related headers
            if (method_exists($server, 'send_headers')) {
                $server->send_headers($this->get_cache_control_headers());
            }

            // Prevent processing multiple requests
            if (true == $this->lock) {
                return $result;
            }

            if ($method != 'POST') {

                $cache_key = 'rest_enhancer' . apply_filters('rest_enhancer_cache_key', $request_uri, $server, $request);

                if (false === ($result = get_transient($cache_key))) {

                    if (!$this->lock) {
                        $this->lock = true;
                    }

                    $result = $server->dispatch($request);
                    $timeout = apply_filters('rest_enhancer_cache_expiration', 5 * MINUTE_IN_SECONDS);

                    set_transient($cache_key, $result, $timeout);

                    // Extends method via hook
                    do_action('rest_enhancer_response_cached', $cache_key, $request_uri, $request);
                }

            }
            return $result;
        }

        /**
         * Returns cache control related headers
         * @return array
         */
        private function get_cache_control_headers(): array
        {
            return apply_filters('rest_enhancer_cache_control_headers', $this->cache_control_headers);
        }
    }

}