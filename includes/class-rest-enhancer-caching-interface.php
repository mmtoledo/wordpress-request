<?php
/**
 * @package rest_enhancer
 * @author Teravision Technologies
 * @version 1.0
 */

namespace rest_enhancer;

/**
 * Interface rest_enhancer_Caching_Interface
 */
interface Rest_Enhancer_Caching_Interface {

    /**
     * Filters wordpress rest_pre_dispatch hook to
     * handle endpoint responses caching
     * @param $result
     * @param $server
     * @param $request
     *
     * @return mixed
     */
    public function rest_enhancer_pre_dispatch($result, $server, $request);


}