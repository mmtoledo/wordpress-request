<?php
/**
 * @package rest_enhancer
 * @author Teravision Technologies
 * @version 1.0
 */
namespace rest_enhancer;

use \WP_REST_Response;
use \WP_REST_Server;
use \WP_REST_Request;

if ( ! class_exists( 'Rest_Enhancer_Logger' ) ) {
    /**
     * Class to stores logs of HTTP errors on WordPress REST API requests.
     */
    class Rest_Enhancer_Logger
    {
        /**
         * Log level constants.
         */
        const LOG_INFO = 'INFO';
        const LOG_WARNING = 'WARNING';
        const LOG_ERROR = 'ERROR';

        /**
         * Enhancer_Logger constructor.
         */
        public function __construct() {}

        /**
         * Filters HTTP errors of the request result and writes this data to log file.
         *
         * @param WP_REST_Response $result Result to send to the client.
         * @param WP_REST_Server $server Server instance.
         * @param WP_REST_Request $request
         *
         * @return WP_REST_Response Request used to generate the response.
         */
        public function http_errors(WP_REST_Response $result, WP_REST_Server $server, WP_REST_Request $request) {
            if ($result instanceof WP_REST_Response) {
                $status = $result->get_status() ?? false;

                if ($status >= 400) { // Error logs.
                    $data = $result->get_data();

                    $msg = is_array( $data ) || is_object( $data )
                        ? wp_json_encode( $result->get_data() )
                        : $result->get_data();

                    self::set_log( $msg, 'error-log', self::LOG_ERROR );

                } else { // Info logs
                    $path = $request->get_route();
                    $time = Rest_Enhancer_Utils::get_request_time();
                    $msg  = $path .' | '. $time . 'ms.';

                    self::set_log( $msg, 'access-log', self::LOG_INFO );
                }
            }

            return $result;
        }

        /**
         * Sets application logs.
         *
         * @param string $message Message of log.
         * @param string $file_name File destination.
         * @param string $log_level Log level (info, error, warning).
         */
        public static function set_log( string $message, string $file_name, string $log_level ) {
            if ( function_exists( 'error_log' ) ) {
                $timestamp = '['.date('Y-m-d H:i:s (T)').']-';

                $folder = REST_ENHANCER_PATH . '/logs/';

                if ( ! file_exists( $folder ) ) {
                    mkdir( $folder, 0755, true );
                }

                $path = $folder . $file_name . date( 'Y-m-d' ) . '.log';

                error_log( $timestamp . "[{$log_level}] " . $message . PHP_EOL, 3, $path );
            }
        }

        /**
         * Run.
         */
        public function load() {
            add_filter('rest_post_dispatch', [$this, 'http_errors'], 10, 3);
        }
    }
}