<?php
/**
 * @package rest_enhancer
 * @author Teravision Technologies
 * @version 1.0
 */
namespace rest_enhancer;

/**
 * Class rest_enhancer_Utils
 * Set of utilities
 * @package rest_enhancer
 */
class Rest_Enhancer_Utils {


    /**
     * Gets the $_SERVER index values, using secure method.
     *
     * @param $varName
     *
     * @return mixed
     */
    static function get_server_var($varName) {
        return filter_input(INPUT_SERVER, $varName);
    }


    /**
     * Return the time of request execution in milliseconds.
     *
     * @return float
     */
    static function get_request_time() {
        // filter_input REQUEST_TIME_FLOAT bug.
        // @see https://bugs.php.net/bug.php?id=61497&edit=1
        $request_time = $_SERVER['REQUEST_TIME_FLOAT'];
        $current_time = microtime(true);

        return round( ($current_time - $request_time) / 1000, 3);
    }

}