# WP REST API Enhancer


Enable caching HTTP requests for a default of 5 minutes, logs HTTP errors and request times in a performant and secure manner. Extensible via actions and filters using modern PHP standards and language features.

-  [Installation](#installation)

-  [Filters](#filters)

-  [How to use filters](#how-to-use-filters)

-  [Actions](#actions)

-  [How to use actions](#how-to-use-actions)

  

## Installation

Here goes the information for the installation using composer. 

  

## Filters

| Filter | Argument(s) | Description |
|--|--|--|
| rest_enhancer_cache_key | string `$request_uri`, WP_REST_Server `$server`, WP_REST_Request `$request` | Allows changing the name/key used to caching a responses |
| rest_enhancer_cache_control_headers | array `cache_control_headers` | Allows to modify control cache header value |
| rest_enhancer_cache_expiration | int `seconds` | Allows to set a different cached expiration time (default 5 minutes) |




## How to use filters




-  **changing the cache timeout**



```PHP

add_filter( 'rest_enhancer_cache_expiration', function() {

// https://codex.wordpress.org/Transients_API#Using_Time_Constants

return  30 * MINUTE_IN_SECONDS;

} );

```



-  **changing cache key name (name used to store cached response)**



```PHP

add_filter( 'rest_enhancer_cache_key', function($currentKey, $server, $request) {
    return "my_custom_key";
} );

```


  
## Actions

| Action | Argument(s) | Description |
|--|--|--|
| rest_enhancer_response_cached | string `$request_uri`, WP_REST_Server `$server`, WP_REST_Request `$request` | Fires right after the response has been cached |



## How to use actions



```PHP

function my_custom_action($cache_key, $request_uri, $request){
    ... do some custom actions
}

add_action( 'rest_enhancer_response_cached', 'my_custom_action', 10, 3);

```